// const inbox = require("./inbox");
// const db = require("./index");
// const Conversation = require("./Conversation");
// const conn = require('../conn');
// const { Sequelize } = conn;
module.exports = (sequelize, DataTypes) => {
  const message = sequelize.define("message", {
    sender: {
      type: DataTypes.INTEGER,
      reference: {
        model: "user",
        key: "id",
      },
    },
    receiver: {
      type: DataTypes.INTEGER,
      reference: {
        model: "user",
        key: "id",
      },
    },
    text: DataTypes.TEXT,
    inbox_id: {
      type: DataTypes.INTEGER,
      reference: {
        model: "inbox",
        key: "id",
      },
    },
  });
  return message;
};
