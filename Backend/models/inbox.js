const db = require(".");
const user = require("./user");
// const conn = require("../conn");
// const { Sequelize } = conn;
// const { Op } = Sequelize;

module.exports = (sequelize, DataTypes) => {
  const inbox = sequelize.define("inbox", {
    sender: DataTypes.INTEGER,
    receiver: DataTypes.INTEGER,
    last_msg: DataTypes.TEXT,
    seen: DataTypes.STRING,
    deleted: DataTypes.STRING,
  });
  return inbox;
};
