module.exports = (sequelize, DataTypes) => {
  const skills = sequelize.define(
    "skills",
    {
      skills: DataTypes.STRING,
    },
    {
      timestamps: false,
    }
  );
  return skills;
};
