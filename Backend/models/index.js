"use strict";

const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");

const basename = path.basename(__filename);
const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "/../config/config.json")[env];
const db = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );
}

fs.readdirSync(__dirname)
  .filter((file) => {
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach((file) => {
    const model = require(path.join(__dirname, file))(
      sequelize,
      Sequelize.DataTypes
    );
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

const user = require("./user")(sequelize, Sequelize);
const jobpost = require("./jobpost")(sequelize, Sequelize);
const services = require("./services")(sequelize, Sequelize);
const order = require("./order")(sequelize, Sequelize);
const skills = require("./skills")(sequelize, Sequelize);
const userskill = require("./userskill")(sequelize, Sequelize);
const message = require("./Message")(sequelize, Sequelize);
const inbox = require("./inbox")(sequelize, Sequelize);
db.user.hasMany(db.jobpost, { as: "users" });

// user.belongsToMany(skills, { through: "userskill" });
// skills.belongsToMany(user, { through: "userskill" });


user.hasMany(message, {
  foreignKey: "sender",
});

user.hasMany(message, {
  foreignKey: "receiver",
});

// inbox.hasMany(message, {
//   foreignKey: "inbox_id",
// });

user.hasMany(userskill);
skills.hasMany(userskill);

user.hasMany(jobpost);
user.hasMany(services);

// inbox.hasMany(message, {
//   sourceKey: "inbox_hash",
//   foreignKey: "inbox_hash",
//   constraints: true,
// });

// message.hasMany(inbox, {
//   foreignKey: "inbox_hash",
//   sourceKey: "inbox_hash",
//   as: "inboxHash",
//   constraints: false,
// });

user.hasMany(inbox, {
  foreignKey: "sender",
});
user.hasMany(inbox, {
  foreignKey: "receiver",
});
inbox.belongsTo(user, {
  foreignKey: "sender",
});
inbox.belongsTo(user, {
  foreignKey: "receiver",
});
// inbox.belongsTo(user, {
//   foreignKey: "sender",
// });
// inbox.belongsTo(user, {
//   foreignKey: "receiver",
// });

// user.hasMany(inbox, {
//   foreignKey: "last_senderid",
// });

user.hasMany(order, {
  foreignKey: "userID",
});
jobpost.hasMany(order, {
  foreignKey: "Jobpost_Id",
});
user.hasMany(order, {
  foreignKey: "Applied_user",
});
user.hasMany(order, {
  foreignKey: "From_user",
});
user.hasMany(order, {
  foreignKey: "To_user",
});
services.hasMany(order);

module.exports = db;
