const AdminBro = require('admin-bro')
const AdminBroExpress = require('admin-bro-expressjs')
const AdminBroSequelize = require('admin-bro-sequelizejs')
const db = require("../models")

AdminBro.registerAdapter(AdminBroSequelize)

const adminBro = new AdminBro({
  databases: [db],
  rootPath: '/admin',
})
const ADMIN = {
  email: process.env.ADMIN_EMAIL || 'skilllib@123.com',
  password: process.env.ADMIN_PASSWORD || 'skilllib',
}

const router = AdminBroExpress.buildAuthenticatedRouter(adminBro, {
  authenticate: async (email, password) => {
    if (email === ADMIN.email && password === ADMIN.password){
      return ADMIN
    }
    return null
  },
  cookieName: process.env.ADMIN_COOKIE_NAME || 'admin-bro',
  cookiePassword: process.env.ADMIN_COOKIE_PASS || 'supersecret-and-long-password-for-cookie-in-the-browser',
})

module.exports = router;