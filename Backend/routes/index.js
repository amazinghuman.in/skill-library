var express = require("express");
var router = express.Router();
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const verify = require("../controllers/GoogleLoginRN");

const { NotFound } = require("http-errors");
const { body, validationResult } = require("express-validator");

const { ensureAuth, ensureGuest } = require("../middleware/gAuth");

const db = require("../models/index");
const user = require("../models/user");
const User = db.user;

const Jobpost = db.jobpost;
const Inbox = db.inbox;

const { sequelize } = require("../models/index");
const { query } = require("express");
const { where, json } = require("sequelize");
const Skill = db.skills;

router.get("/test", async (req, res) => {
  let userId = 1;

  const deviceToken = await User.findAll(
    {
      attributes: ["fcm_Token"],
      raw: true,
    },
    {
      where: {
        id: userId,
      },
    }
  );
  console.log("FCM TOKEN ->", deviceToken[0].fcm_Token);
  res.json(deviceToken);
});

router.post("/fcmToken", async (req, res) => {
  // console.log(req.body);
  const fcm_Token = req.body.FCM_Token;
  const userId = req.body.UserId;
  // console.log("fcm_Token -> ", fcm_Token, "\n", userId);
  const tokenUpdated = await User.update(
    {
      fcm_Token: fcm_Token,
    },
    { where: { id: userId } }
  ).catch((e) => console.log(e));
  res.json(tokenUpdated);
});

// skills list routes
router.get("/skills", async function (req, res, next) {
  const skillslist = await Skill.findAll({
    order: Sequelize.literal("rand()"),
    limit: 10,
  });
  console.log(skillslist);
  res.json(skillslist);
});

// nearby jobs location search route
router.post("/search", async function (req, res, next) {
  const { Id, skill, long, lat } = req.body;

  console.log(Id);
  console.log(skill);
  console.log(long);
  console.log(lat);
  const skilltable = await Skill.findOne({
    where: {
      skills: {
        [Op.like]: skill + "%",
      },
    },
  });

  try {
    const skillid = skilltable.id;
    console.log(skillid);

    const searchresults =
      await sequelize.query(`SELECT userskills.userId, users.name, users.Tagline,users.Work_Description,users.profile_photo, ( 6371 * 
    acos( cos( radians(${lat}) )* 
    cos( radians( latitude ) ) * 
    cos( radians( longitude ) - 
    radians(${long}) ) + 
    sin( radians(${lat}) ) * 
    sin( radians( latitude ) ) ) ) AS distance FROM users
INNER JOIN userskills
ON (users.id = userskills.userId and userskills.skillId = ${skillid})
where (users.id != ${Id})
    HAVING (distance < 5)
    ORDER BY distance LIMIT 0 , 20;`);
    //console.log(searchresults.length);
    //console.log(searchresults[0].length);
    //console.log(searchresults);
    if (searchresults[0].length > 0) {
      res.json({
        info: `Results for ${skilltable.skills} users in your location`,
        SkillResult: searchresults[0],
      });
    } else if (searchresults[0].length == 0) {
      const search_without_location =
        await sequelize.query(`SELECT userskills.userId, users.name, users.Tagline,users.Work_Description,users.profile_photo FROM users
      INNER JOIN userskills
      ON (users.id = userskills.userId and userskills.skillId = ${skillid})
      where (users.id != ${Id});`);
      if (search_without_location[0].length > 0) {
        res.json({
          info: `Results for ${skilltable.skills} users`,
          SkillResult: search_without_location[0],
        });
      } else if (search_without_location[0] == 0) {
        const random_users = await User.findAll(
          {
            attributes: [
              "id",
              "name",
              "Tagline",
              "Work_Description",
              "profile_photo",
            ],
          },
          { order: Sequelize.literal("rand()"), limit: 10 },
          {
            where: {
              Id: {
                [Op.ne]: User.id,
              },
            },
          }
        );
        res.json({
          info: `No results for ${skilltable.skills}, Showing random skilled people `,
          SkillResult: random_users,
        });
      }
    }
  } catch (error) {
    console.log(error);

    res.json({ info: "Sorry Technical Problem" });
  }
});

/* GET home route. */
router.get("/", function (req, res, next) {
  res.json({ "Skill library": "Connected" });
});

//To verify google logins by TokenID
router.post("/google/verify", (req, res) => {
  const Token = req.body.idToken;
  verify(Token)
    .then((result) => {
      console.log(result.jwtToken);
      res.json({ JWTToken: result.jwtToken });
    })
    .catch((err) => {
      console.log(err);
      res.send("failed");
    });
});

router.get("/logout", (req, res) => {
  req.logOut();
  res.redirect("/");
});

//skill autocomplete query route
router.get("/skillquery/:sq", async (req, res) => {
  const query = req.params.sq;
  const skillList = await Skill.findAll({
    limit: 3,
    where: {
      skills: {
        [Op.like]: query + "%",
      },
    },
  });
  // const skillsArr = [];
  // skillList.forEach((crrVal) => {
  //   skillsArr.push(crrVal.skills);
  // });

  res.json({ skillsArr: skillList });
});

router.get("/inboxID/:sender/:receiver", async (req, res) => {
  const { sender, receiver } = req.params;
  const chkInbox = await Inbox.findOne({
    where: {
      [Op.or]: [
        {
          [Op.and]: [{ sender: sender }, { receiver: receiver }],
        },
        {
          [Op.and]: [{ receiver: sender }, { sender: receiver }],
        },
      ],
    },
  }).catch((err) => console.log(err));

  if (chkInbox) {
    res.json({ Inboxid: chkInbox.dataValues.id });
  } else {
    res.json({ Inboxid: 0 });
  }
});

// new jobbpost

router.post("/newjobpost", async function (req, res, next) {
  const UserId = req.body.id;
  const Title = req.body.Title;
  const Description = req.body.Description;

  const jobpost = await sequelize
    .query(
      `INSERT INTO ahbjobs.jobposts (Title,Description,createdAt,updatedAt, userId) VALUES ('${Title}','${Description}',NOW(),NOW(), ${UserId})`
    )
    .catch((err) => console.log(err));
  res.status(200).json(jobpost);
});

// Message seen
router.put("/message/readed/:id", async (req, res) => {
  const inboxID = req.params.id;
  const messageReaded = await Inbox.update(
    { seen: 1 },
    {
      where: { id: inboxID },
    }
  ).catch((e) => console.log(e));
  res.json(messageReaded);
});

// edit/update jobpost route

router.put("/jobpostupdate/:id", async function (req, res, next) {
  const id = req.params.id;
  const updatedJobpost = req.body;
  console.log("updated jobpost", req.body);
  const updateJobpostDB = await Jobpost.update(updatedJobpost, {
    where: { id: id },
  }).catch((err) => {
    console.log(err);
  });
  res.json(updateJobpostDB);
});

// delete jobpost route

router.delete("/jobpostdelete/:id", async function (req, res, next) {
  const id = req.params.id;
  const deleteJobpost = await Jobpost.destroy({ where: { id: id } }).catch(
    (err) => {
      console.log(err);
    }
  );
  res.json(deleteJobpost);
});

router.get("/jobpost/:id", async (req, res, next) => {
  const userId = req.params.id;
  const jobposts =
    await sequelize.query(`SELECT jobposts.id,jobposts.Title,jobposts.Description, users.name, users.profile_photo FROM users
  INNER JOIN jobposts
  ON (users.id = jobposts.userId )
  where (users.id != ${userId});`);
  res.json(jobposts);
});

module.exports = router;
