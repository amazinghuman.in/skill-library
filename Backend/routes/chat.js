// const server = require("http").createServer().listen(3000);
const { WebSocketServer } = require("ws");

const io = new WebSocketServer({ port: 3001 });

// const app = require("../app");
// let io = app.get("io");
// const io = require("../bin/www");
const user = require("../models/user");
const Conversation = require("../models/Conversation");
const Message = require("../models/Message");
// conn.sync({ logging: false, force: true });
const mobileSockets = {};

io.on("connection", (socket) => {
  socket.on("newUser", (credentials) => {
    const { phone } = credentials;
    Promise.all([
      user.findOrCreate({
        where: {
          phone,
        },
      }),
      user.findAll(),
    ]).then(([user, users]) => {
      mobileSockets[user[0].id] = socket.id;
      socket.emit("userCreated", { user: user[0], users });
      socket.broadcast.emit("newUser", user[0]);
    });
  });

  socket.on("chat", (users) => {
    Conversation.findOrCreateConversation(
      users.user.id,
      users.receiver.id
    ).then((conversation) =>
      socket.emit("priorMessages", conversation.messages)
    );
  });
  //
  socket.on("message", ({ text, sender, receiver }) => {
    Message.createMessage(text, sender, receiver).then((message) => {
      socket.emit("incomingMessage", message);
      const receiverSocketId = mobileSockets[receiver.id];
      socket.to(receiverSocketId).emit("incomingMessage", message);
    });
  });
});

module.exports = io;
